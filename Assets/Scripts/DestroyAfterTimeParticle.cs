﻿using System.Collections;
using UnityEngine;

public class DestroyAfterTimeParticle : MonoBehaviour {
	[Tooltip("Уничтожение эффектов")]
	public float timeToDestroy = 0.8f;

	void Start () {
		Destroy (gameObject, timeToDestroy);
	}

}
