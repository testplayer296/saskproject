using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Interactive : MonoBehaviour
{
	public Transform _cam;
	public Transform[] floor;
	public Transform elevator;
	public float speed = 1f;
	RaycastHit hit;
	public float range = 2f;
	public GameObject _text;
	[Header("PanelDiolog")]
	public GameObject panelDiolog;
	string npc;
	public Text tnpc;
	public GameObject ButtonQe;
	public Text tplayer;
	string player;
	public GameObject[] uI;
	public float i = 0f;


	void Update()
	{
		Elevatorr();
		NPC();
	}

	void Elevatorr()
	{
		if (Physics.Raycast(_cam.position, _cam.forward, out hit, range))
		{
			float step = speed * Time.deltaTime;
			if (hit.transform.tag == "Ponel")
			{
				_text.SetActive(true);
			}
			else
			{
				_text.SetActive(false);
			}

			if (hit.transform.tag == "floor0" && Input.GetKey(KeyCode.E))
			{
				_text.SetActive(true);
				elevator.position = Vector3.MoveTowards(elevator.position, floor[0].position, step);
			}
			if (hit.transform.tag == "floor1" && Input.GetKey(KeyCode.E))
			{
				_text.SetActive(true);
				elevator.position = Vector3.MoveTowards(elevator.position, floor[1].position, step);
			}
			if (hit.transform.tag == "floor2" && Input.GetKey(KeyCode.E))
			{
				_text.SetActive(true);
				elevator.position = Vector3.MoveTowards(elevator.position, floor[2].position, step);
			}
			if (hit.transform.tag == "floor3" && Input.GetKey(KeyCode.E))
			{
				_text.SetActive(true);
				elevator.position = Vector3.MoveTowards(elevator.position, floor[3].position, step);
			}
			if (hit.transform.tag == "floor4" && Input.GetKey(KeyCode.E))
			{
				_text.SetActive(true);
				elevator.position = Vector3.MoveTowards(elevator.position, floor[4].position, step);
			}
			if (hit.transform.tag == "floor5" && Input.GetKey(KeyCode.E))
			{
				_text.SetActive(true);
				elevator.position = Vector3.MoveTowards(elevator.position, floor[5].position, step);
			}
		}


		if (hit.transform == null)
		{
			_text.SetActive(false);
		}
	}

	public void Proverka()
	{
		i = i + 1f;
	}

	public void ButtonQ()
	{
		panelDiolog.SetActive(false);
		uI[1].SetActive(true);
		uI[0].SetActive(true);
		Time.timeScale = 1f;
	}

	void NPC()
	{
		if (Physics.Raycast(_cam.position, _cam.forward, out hit, range))
		{
			if (hit.transform.tag == "NPC")
			{
				_text.SetActive(true);
				if (Input.GetKey(KeyCode.E))
				{
					panelDiolog.SetActive(true);
					uI[1].SetActive(false);
					uI[0].SetActive(false);
					Time.timeScale = 0f;
				}

			}

			if (hit.transform == null)
			{
				_text.SetActive(false);
			}
			if (Input.GetKeyDown(KeyCode.Escape))
			{
				ButtonQ();
			}

		}

	}
	public void Hello()
	{
		if (i == 0f)
		{
			npc = "���� - �����, ���� ������ ����� �� ������ �� ������ ��� �����, ��� � ���� ������.";
			tnpc.text = npc;
			player = "�� - ��� �� � ��� ���������� � ����.";
			tplayer.text = player;
		}
		if (i == 1f)
		{
			npc = "���� - ����� ���� ������ ����������(��), ��� 60 ���. � ���� �� ��� ���� �������, ���� ����� ��� ������� ������� � �� ��� ������, ��������..";
			tnpc.text = npc;
			player = "�� - �������� ��� ��� � ������ ������������, ��� ��������� ";
			tplayer.text = player;
		}
		if (i == 2f)
		{
			npc = "����, �� ����� ���� �������� �����, �� ��� � ������������, ������ �����, ��� ������ ������.";
			tnpc.text = npc;
			player = "�� - �������. ����� � ����� ����� ";
			tplayer.text = player;
		}
		if (i == 3f)
		{
			ButtonQe.SetActive(true);
			ButtonQ();
		}
	}
}
