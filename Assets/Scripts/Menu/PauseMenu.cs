using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
	public static bool GameIsPaused = false;

	public GameObject pauseMenuUI;
	public GameObject detMenuUI;
	public Image hpBar;
	public float hpAmount = 1f;

	void Update()
	{
		hpAmount = hpBar.fillAmount;
		Pausemenu();
		GameMenu();
		Cursr();
	}


	void Pausemenu()
	{
		if (Input.GetKeyDown(KeyCode.Escape))
		{
			if (GameIsPaused)
			{
				Resume();
			}
			else
			{
				Pause();
			}
		}
	}

	public void GameMenu()
	{
		if (hpAmount == 0f)
		{
			detMenuUI.SetActive(true);
			Time.timeScale = 0f;
			if (Input.GetKeyDown(KeyCode.Space))
			{
				SceneManager.LoadScene(0);
			}
		}
	}

	public void Resume()
	{
		pauseMenuUI.SetActive(false);
		Time.timeScale = 1f;
		GameIsPaused = false;
	}

	void Pause()
	{
		pauseMenuUI.SetActive(true);
		Time.timeScale = 0f;
		GameIsPaused = true;

	}

	public void Menu()
	{
		SceneManager.LoadScene(0);
	}
	public void ExitGame()
	{
		Application.Quit();
	}

	void Cursr()
	{
		if (Time.timeScale == 0f)
		{
			Cursor.visible = true;
		}
		else
		{
			Cursor.visible = false;
		}
	}
}