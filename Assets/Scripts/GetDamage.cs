using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GetDamage : MonoBehaviour
{

    public float flag = 0;
    public float Damag = 20f;
    public float hpAmount = 100f;
    public float stAmount = 100f;

    public Image hpBarBot;

    public float speed = 200f;
    public float rand;

    public GameObject bot;
    public Transform Player;

    public float time = 1;

    public float check;
    public Rigidbody rb;

    public float distantion;

    public Vector3 pPos;
    public Vector3 bPos;

    private IEnumerator Routine;
    private float countRouteine = 1;
    private float botTrigger = 0;

    public Vector3 heading;
    public float distance;
    public Vector3 direction;

    void Start()
    {
        Routine = MoveBots();
        rb = GetComponent<Rigidbody>();
        //StartCoroutine(Routine);
        Player = GameObject.Find("MainPlayer").transform;
    }

    void BotAttack()
    {
        bot.transform.rotation = Player.rotation;
        bot.transform.Rotate(0f, -90f, 0f, Space.Self);
        bot.transform.position -= transform.right * 2f * Time.deltaTime;
        if (Random.Range(0, 2) == 0) {
            //bot.transform.position += transform.forward * 1f * Time.deltaTime;
        } else
        {
            //bot.transform.position -= transform.forward * 1f * Time.deltaTime;
        }
        
        //bot.transform.rotation = Quaternion.Euler(Player.rotation.x, Player.rotation.y, Player.rotation.z);
    }


    void Update()
    {
        heading = bot.transform.position - Player.position;
        distance = heading.magnitude;
        direction = heading / distance;

        if (hpAmount == 0)
        {
            Destroy(gameObject);
        }

        distantion = Vector3.Distance(Player.position, bot.transform.position);

        if (distantion > 20)
        {
            botTrigger = 0;
        }

        if (distantion < 10 || botTrigger == 1)
        {
            speed = 300f;
            time = 0.5f;
            if (countRouteine == 0)
            {
                StopCoroutine(Routine);
            }
            countRouteine = 1;
            BotAttack();


        } else
        {
            speed = 200f;
            time = 1f;
            if (countRouteine == 1)
            {
                StartCoroutine(Routine);
            }
            countRouteine = 0;
        }
        pPos = Player.transform.position;
        bPos = bot.transform.position;
    }

    IEnumerator MoveBots()
    {
        for(int i = 0; i < 120; i++)
        {       
            MoveBot();
            yield return new WaitForSeconds(time);
        }     
    }

    void MoveBot()
    {
        rand = Random.Range(0, 8);
        if (rand == 0)
        {
            //bot.transform.localPosition += transform.forward * speed;
            rb.AddForce(transform.forward * speed);
            check++;
        }

        if (rand == 1)
        {
            //bot.transform.localPosition += -transform.forward ;
            rb.AddForce(-transform.forward * speed);
            check++;
        }

        if (rand == 2)
        {
            //bot.transform.localPosition += transform.right;
            rb.AddForce(transform.right * speed);
        }

        if (rand == 3)
        {
           // bot.transform.localPosition += -transform.forward;
            rb.AddForce(-transform.right * speed);
        }

        if (rand == 4)
        {
            bot.transform.Rotate(0f, 45f, 0f, Space.Self);
        }

        if (rand == 5)
        {
            bot.transform.Rotate(0f, -45f, 0f, Space.Self);
        }
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Bullet")
        {
            flag++;
            hpAmount -= Damag;
            hpBarBot.fillAmount = hpAmount / 100;
            botTrigger = 1;
        }

        if (collision.gameObject.tag == "Player")
        {
            flag++;
        }
    }
}
