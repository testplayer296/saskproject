using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class BotLogic : MonoBehaviour
{

    public GameObject bot;
    public GameObject Player;

    public float xPos;
    public float zPos;

    public float rand;

    public float testrand;
    void Start()
    {
               
    }
    void Update()
    {       
        GetInput();
    }

    private void GetInput()
    {
        if (Input.GetKeyDown(KeyCode.B))
        {
            StartCoroutine(SpawnBot()); 
        }

/*        if (Input.GetKeyDown(KeyCode.C))
        {
            StopCoroutine(SpawnBot());
        }*/
    }

    IEnumerator SpawnBot()
    {
        for(int i = 0; i < 3; i++)
        {
            RandomDistance();
            yield return new WaitForSeconds(1);
            Instantiate(
                bot,
                new Vector3(Player.transform.position.x + xPos, Player.transform.position.y, Player.transform.position.z + zPos),
                Quaternion.Euler(new Vector3(0, 0, 0))
                );
        }
    }

    void RandomDistance()
    {
        testrand = Random.Range(0, 5);

        if (Random.Range(0, 2) == 0)
        {
            xPos = Random.Range(5, 10);
        } else
        {
            xPos = Random.Range(-10, -5);
        }

        if (Random.Range(0, 2) == 1)
        {
            zPos = Random.Range(5, 10);
        }
        else
        {
            zPos = Random.Range(-10, -5);
        }
    }

}
