using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Crosshair : MonoBehaviour
{
    public RectTransform crosshair;
    [SerializeField] float sizeStop;
    [SerializeField] float sizeStart;
    [SerializeField] float sizeCurrent;

    void Update()
    {
        if (Input.GetAxis("Horizontal") != 0 || Input.GetAxis("Vertical") != 0)
        {
            sizeCurrent = Mathf.Lerp(sizeCurrent, sizeStart, 5 * Time.deltaTime);
        }
        else
        {
            sizeCurrent = Mathf.Lerp(sizeCurrent, sizeStop, 5 * Time.deltaTime);
        }

        if (Input.GetMouseButton(0))
        {
            sizeCurrent = Mathf.Lerp(sizeCurrent, sizeStart, 5 * Time.deltaTime);
        }

        crosshair.sizeDelta = new Vector2(sizeCurrent, sizeCurrent);
    }
}
