
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShotGan : MonoBehaviour
{
	public float damage; //дамаг оружия 
	public float speadShot; // скорост стрельбы
	public float force = 150f; // сила патрона
	public float range; //дальность стрельбы 'райкаста' 
	//public ParticleSystem muzzleFlash; //Эфект выстрела
	public GameObject muzzleFlash; //Эфект выстрела
	public Transform bulletSpawn;
	public LayerMask layerMask;// sloi popadania
	public AudioClip shotSFX; // звук
	public AudioSource _audioSource; // источник звука добавляем аудио сорс
//	public Camera _cam; // переменая типа камера для райкаста сюда камепу
	private float nextFire = 0f; // проверка для стрельбы
	public GameObject hitEffect; // эфект попадания
	public float timeHitEffect = 2f; // длительность эфекта попадания
	public float distantion;
	public GameObject Player;
	public GameObject bullet;
	public GameObject bulletHelp;
	[Header("Переменые Разброс")]
	public float spread = 0f;
	Transform startPoin;
	public Transform fierPoint;
	public float maxSpread = 1f;

	void Start()
	{
		startPoin = fierPoint;
	} 
	 
	void Update()
	{
		distantion = Vector3.Distance(transform.position, Player.transform.position);
		if (distantion < 1.5f && Time.timeScale == 1f)
		{
			Faer();
		}
	}

	void Faer()
	{
		if (Input.GetMouseButton(0) && Time.time > nextFire)// проверка для стрельбы
		{
			nextFire = Time.time + 1f / speadShot;// проверка для стрельбы
			Shote();
		}

	}

	void Shote()
	{
		_audioSource.PlayOneShot(shotSFX); // звуковой эфект вытрела
		//muzzleFlash.Play(); // визуальный эффект выстрела перетащить на дуло
		Instantiate(muzzleFlash,bulletSpawn.position,bulletSpawn.rotation);

		RaycastHit hit; // обявляем переменую для самого рейкаста 

		if (Physics.Raycast(fierPoint.position,fierPoint.forward,out hit,range,layerMask))
		{
			if (hit.transform.tag == "Mob")
			{
				bulletHelp = Instantiate(bullet, hit.point, Quaternion.LookRotation(hit.normal));
				Destroy(bulletHelp, timeHitEffect);// эфект попадания и длительность
			}
			else
			{
				GameObject impact = Instantiate(hitEffect, hit.point, Quaternion.LookRotation(hit.normal));
				Destroy(impact, timeHitEffect);// эфект попадания и длительность
			}
			// Разброс.....
			if( spread < maxSpread)
			{
				spread += 0.1F;
			}
			if (Input.GetMouseButtonDown(0))
			{
				spread = 0f;
			}
				fierPoint.transform.localRotation = Quaternion.identity;
				fierPoint.transform.localRotation = Quaternion.Euler(startPoin.localRotation.x + Random.Range(-spread, spread), 
				startPoin.localRotation.y + Random.Range(-spread, spread), fierPoint.localRotation.z + Random.Range(-spread, spread));
			//.....
			if (hit.rigidbody !=null) // иф силы
			{
				hit.rigidbody.AddForce(-hit.normal * force);
			}

		}

	}
}
