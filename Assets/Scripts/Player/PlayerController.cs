using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
	[Header("�������� ����������� ���������")]
	public float speed = 7f;
	public float runSpeed = 14f;

	public float jumpPower = 100f;
	public float movePower = 2f;

	public bool ground;

	public Rigidbody rb;
	public Rigidbody rbArm;

	public Vector3 directVector;

	public float counterJump = 0;

	public float distantion;
	public GameObject ak;
	public GameObject cam;
	public bool collisArm;
	public bool helperArm;

	[Header("��������� Ponel Status")]
	// ��������� Ponel Status....
	public Image hpBar, stBar;
	public float stamtime = 10f;
	public float Hill = 20f;
	public float Damag = 20f;
	public float hpAmount = 100f;
	public float stAmount = 100f;
	//....

	//test
	public float xposAk;
	public float yposAk;
	public float zposAk;

	public float xRot;
	public float yRot;
	public float zRot;

	public float camPos;

	public bool test;

	public float h;
	public float v;
	public Vector3 dirVector;

	private void Start()
	{
		test = true;
		rbArm = ak.GetComponent<Rigidbody>();

		// �������������� Ponel Status ....
		stBar.fillAmount = stAmount / 100f; 
		hpBar.fillAmount = hpAmount / 100f; 
		//....
	}

	void Update()
	{
		camPos = cam.transform.rotation.x;
		//float h = Input.GetAxis("Horizontal");
		//float v = Input.GetAxis("Vertical");
		//directVector = new Vector3(-v, 0, h);
		//rb.velocity = directVector * 3f;
		//�������� �� ��������.....
		Cit();
		//....
		GetInput();
		xRot = transform.rotation.x;
		yRot = transform.rotation.y;
		zRot = transform.rotation.z;
		//
		//PlayGame();
	}

	private void GetInput()
	{
		// STAMINA
		// ������� � ��������� ����� (������ ������������� �������� ����������)
		if (stAmount < 100)
		if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.D))
		{
			stAmount += 100 / (stamtime + 35) * Time.deltaTime;
			stBar.fillAmount = stAmount / 100;
		}
		else
		{
			stAmount += 100 / (stamtime + 15) * Time.deltaTime;
			stBar.fillAmount = stAmount / 100;
		}
		//
		if (Input.GetKey(KeyCode.W))
        {
			transform.localPosition += transform.forward * speed * Time.deltaTime;
			//rb.AddForce(transform.forward * 30f);
			rb.velocity = directVector * 2f;
			// STAMINA
			if (stAmount > 0)
			{ 
				if (Input.GetKey(KeyCode.LeftShift))
				{
					transform.localPosition += transform.forward * runSpeed * Time.deltaTime;
					//rb.velocity = new Vector3(0f, 0f, 2f);
					//rb.AddForce(transform.forward * movePower);
					// STAMINA
					stAmount -= 100 / stamtime * Time.deltaTime;
					stBar.fillAmount = stAmount / 100;
				}
			}			
        }


		if (Input.GetKey(KeyCode.S))
		{
			transform.localPosition += -transform.forward * speed * Time.deltaTime;

			//rb.AddForce(-transform.forward * movePower);
		}


		if (Input.GetKey(KeyCode.A))
		{
			transform.localPosition += -transform.right * speed * Time.deltaTime;

			//rb.AddForce(-transform.right * movePower);
		}

		if (Input.GetKey(KeyCode.D))
		{
			transform.localPosition += transform.right * speed * Time.deltaTime;
			//rb.AddForce(transform.right * movePower);
		}

/*        if (Input.GetKey(KeyCode.T))
        {
            ak.transform.Rotate(0f, 45f, 0f, Space.Self);
            //ak.transform.rotation = Quaternion.Euler(0, 45, 0);
        }*/

        if (Input.GetKey(KeyCode.Y))
		{
			test = true;
		}

		if (Input.GetKey(KeyCode.U))
		{
			test = false;
		}

		distantion = Vector3.Distance(transform.position, ak.transform.position);

		// ���� � ��������� < 3 ����� �������� && collisArm == true ��� �� ��������� �������� E ����� ������ ����
		if ((collisArm == true || (distantion < 3 && test == true)) && Input.GetKeyDown(KeyCode.E))
		{
			rbArm.constraints = RigidbodyConstraints.FreezeAll;
			ak.transform.SetParent(transform.transform);
			//ak.transform.rotation = Quaternion.Euler(0, 0, 0);

			ak.transform.localRotation = Quaternion.Euler(cam.transform.rotation.x * 100, 0, 0);
			ak.transform.localPosition = new Vector3(0.6f, 0.19f, 1f);
			ak.transform.Rotate(0f, 90f, 0f, Space.Self);
			collisArm = false;
			helperArm = true;
			//ak.transform.position = new Vector3(transform.position.x + 0.6f, transform.position.y + 0.18f, transform.position.z + 1f);
			//ak.transform.rotation = Quaternion.Euler(xRot, yRot, zRot);
		}

		if (collisArm == false && helperArm == true && Input.GetKeyDown(KeyCode.R))
		{
			//ak.transform.rotation = Quaternion.Euler(0f, 0f, 0f);
			ak.transform.SetParent(null);
			rbArm.constraints = RigidbodyConstraints.None;
			rbArm.AddForce(transform.forward * 200f);
			rbArm.AddForce(transform.up * 100f);
			helperArm = false;
			collisArm = false;
		}
			
		//�������� ����� ������ �������� ������ �� bool �������� ���������
		if (counterJump < 2)
		{
			if (stAmount > 0)
			if (Input.GetKeyDown(KeyCode.Space))
			{
				if (ground == true)
				{
					stAmount -= 10f;
					rb.AddForce(transform.up * jumpPower);
					counterJump = counterJump += 1;
				}
				if (ground == false && counterJump == 1)
				{
					stAmount -= 10f;
					rb.AddForce(transform.up * jumpPower);
					counterJump = counterJump += 1;
				}
			}
		}
	}
	private void OnCollisionEnter(Collision collision)
	{
		if (collision.gameObject.tag == "Ground")
		{
			ground = true;
			counterJump = 0f;
		}

		if (collision.gameObject.tag == "Arm")
		{
			collisArm = true;
		}

		if (collision.gameObject.tag == "Mob")
		{
			hpAmount -= Damag;
			hpBar.fillAmount = hpAmount / 100;
		}
	}

	private void OnCollisionExit(Collision collision)
	{
		if (collision.gameObject.tag == "Ground")
		{
			ground = false;

		}

		if (collision.gameObject.tag == "Arm")
		{
			//ground = false;
			//collisArm = false;
		}
	}
	//
	void Cit()
	{
		if (hpAmount > 0)
		{
			if (Input.GetKeyDown(KeyCode.G))
			{
				hpAmount -= Damag;
				hpBar.fillAmount = hpAmount / 100;
			}
		}
		if (hpAmount < 100)
		{
			if (Input.GetKeyDown(KeyCode.H))
			{
				hpAmount += Hill;
				hpBar.fillAmount = hpAmount / 100;
			} 
		}
	}
	//

	//
	private void OnTriggerEnter(Collider other)
	{
		if(other.gameObject.tag == "Deat")
		{
			hpAmount -= Damag;
			hpBar.fillAmount = hpAmount / 100;
			if(hpAmount < 0.001f)
			{
				Deat();
			}
		}
	}

	private void Deat()
	{
		Destroy(gameObject);
	}

//	public void PlayGame()
//	{
//		if (Input.GetKeyDown(KeyCode.Escape))
//		SceneManager.LoadScene(0);
//	}

}