using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    private float mouseX;
    private float mouseY;

    private float xRotation = 0f;

    public Transform Player;

    public float sensetivityMouse = 200f;

    public GameObject kalash;
    public float distationCamera;

    public float helper;
    void Start()
    {
     //  Cursor.lockState = CursorLockMode.Locked;
    }

    void Update()
    {

        distationCamera = Vector3.Distance(transform.position, kalash.transform.position);

        mouseX = Input.GetAxis("Mouse X") * sensetivityMouse * Time.deltaTime;
        mouseY = Input.GetAxis("Mouse Y") * sensetivityMouse * Time.deltaTime;

        xRotation -= mouseY;
        xRotation = Mathf.Clamp(xRotation, -90f, 90f);

        Player.Rotate(mouseX * new Vector3(0, 1, 0));
        if (distationCamera < 2)
        {
            kalash.transform.Rotate(mouseY * new Vector3(0, 0, -1));
        }
        //transform.Rotate(-mouseY * new Vector3(1, 0, 0));
        transform.localRotation = Quaternion.Euler(xRotation, 0f, 0f);
        //kalash.transform.localRotation = Quaternion.Euler(xRotation, 0f, 0f);
    }
}
